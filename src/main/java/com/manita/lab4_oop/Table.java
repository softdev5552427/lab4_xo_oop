/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.manita.lab4_oop;

/**
 *
 * @author user
 */
public class Table {

    private String[][] table = {{"-", "-", "-"}, {"-", "-", "-"}, {"-", "-", "-"}};
    private Player currentPlayer, player1, player2;
    private int row, col;

    public Table(Player player1, Player player2) {
        this.player1 = player1;
        this.player2 = player2;
        this.currentPlayer = player1;
    }

    public String[][] getTable() {
        return table;
    }

    public Player getCurrentPlayer() {
        return currentPlayer;
    }

    public boolean setRowAndCol(int row, int col) {
        if (table[row - 1][col - 1].equals("-")) {
            table[row - 1][col - 1] = currentPlayer.getSymbol();
            this.row = row;
            this.col = col;
            return true;
        }
        return false;
    }

    public boolean checkWin() {
        if (checkRow()) {
            if (currentPlayer == player1) {
                player1.win();
                player2.lose();
            } else {
                player1.win();
                player2.lose();
            }
            return true;
        }
        if (checkCol()) {
            if (currentPlayer == player1) {
                player1.win();
                player2.lose();
            } else {
                player1.win();
                player2.lose();
            }
            return true;
        }
        if (checkX()) {
            if (currentPlayer == player1) {
                player1.win();
                player2.lose();
            } else {
                player1.win();
                player2.lose();
            }
            return true;
        }
        return false;

    }

    public boolean checkDraw() {
        for (int i = 0; i < table.length; i++) {
            for (int j = 0; j < table.length; j++) {
                if (table[i][j].equals("-")) {
                    return false;
                }

            }

        }
        return true;

    }

    private boolean checkRow() {
        for (int j = 0; j < table[row - 1].length; j++) {
            if (!table[row - 1][j].equals(currentPlayer.getSymbol())) {
                return false;
            }
        }
        return true;
    }

    private boolean checkCol() {
        for (int i = 0; i < table[row - 1].length; i++) {
            if (!table[i][col - 1].equals(currentPlayer.getSymbol())) {
                return false;
            }
        }
        return true;
    }

    private boolean checkX() {
        if (row - 1 == col - 1) {
            for (int i = 0; i < table.length; i++) {
                if (!table[i][i].equals(currentPlayer.getSymbol())) {
                    return false;
                }

            }
            return true;
        }
        if ((row + col) - 2 == table.length - 1) {
            for (int i = 0; i < table.length; i++) {
                if (!table[i][table.length - 1 - i].equals(currentPlayer.getSymbol())) {
                    return false;
                }
            }
            return true;
        }
        return false;
    }

    public void switchPlayer() {
        if (currentPlayer == player1) {
            currentPlayer = player2;
        } else {
            currentPlayer = player1;
        }
    }

}
