/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.manita.lab4_oop;

import java.util.Scanner;

/**
 *
 * @author user
 */
public class Game {

    private Table table;
    private Player player1, player2;
    private boolean confirm;

    public Game() {
        player1 = new Player("X");
        player2 = new Player("O");
        table = new Table(player1, player2);
    }

    public void Play() {
        showWelcome();
        while (confirm == true) {
            showTurn();
            showTable();
            inputRowAndCol();
            if (table.checkWin()) {
                showTable();
                showWin();
                printPlayer();
                printPlayAgain();
                resetTable();
            }
            if (table.checkDraw()) {
                showTable();
                showDraw();
                printPlayer();
                printPlayAgain();
                resetTable();
            }
            table.switchPlayer();
        }
    }

    private void showWelcome() {
        Scanner sc = new Scanner(System.in);
        System.out.println("Welcome to XO Game");
        System.out.println("Start XO Games? (Y/N): ");
        String start = sc.nextLine().toUpperCase();
        while (!start.equals("Y") && !start.equals("N")) {
            System.out.println("Start XO Games? (Y/N): ");
            start = sc.nextLine().toUpperCase();
        }
        if (start.equals("N")) {
            confirm = false;
            System.out.println("Goodbye!!!");
        } else {
            confirm = true;
        }
    }

    private void showTable() {
        String[][] t = table.getTable();
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                System.out.print(" " + t[i][j] + " ");
            }
            System.out.println();
        }

    }

    private void showTurn() {
        System.out.println("Turn: " + table.getCurrentPlayer().getSymbol());
    }

    private void inputRowAndCol() {
        Scanner sc = new Scanner(System.in);
        System.out.println("Input row: ");
        int row = sc.nextInt();
        System.out.println("Input column: ");
        int col = sc.nextInt();
        table.setRowAndCol(row, col);
    }

    private void showWin() {
        System.out.println("+--------------------+");
        System.out.println("|    !!! " + table.getCurrentPlayer().getSymbol() + " Win !!!   |");
        System.out.println("+--------------------+");

    }

    private void showDraw() {
        System.out.println("+--------------------+");
        System.out.println("|    !!! Draw !!!    |");
        System.out.println("+--------------------+");
    }

    private boolean printPlayAgain() {
        System.out.print("You want to play again (Y/N) : ");
        Scanner sc = new Scanner(System.in);
        String again = sc.nextLine().toUpperCase();
        while (!again.equals("N") && !again.equals("Y")) {
            System.out.print("You want to play again (Y/N) : ");
            again = sc.nextLine().toUpperCase();
        }
        if (again.equals("N")) {
            confirm = false;
            System.out.println("GoodBye!!");
            return false;
        }
        return true;

    }
    private void resetTable() {
         String[][] t = table.getTable();
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                t[i][j] = "-";
            }
        }
    }

    private void printPlayer() {
        System.out.println(player1);
        System.out.println(player2);
    }

}
